#!/bin/bash

sudo docker run -d \
        -e C_FORCE_ROOT=1 \
        -v $PWD/site:/site \
        --name="celery-beat" \
        iadb/retocid:celery \
        /usr/bin/python /site/retocid/manage.py celery beat \
        --app=retocid \
        --scheduler=djcelery.schedulers.DatabaseScheduler \
        --loglevel=INFO \
        --logfile=/site/retocid/log/beat.log
