#/bin/bash

sudo docker run \
    -d \
    -p 80:80 \
    -v $PWD/site:/site \
    --name="retocid-web" \
    iadb/retocid:v1 \
    /usr/sbin/apache2ctl -D FOREGROUND
