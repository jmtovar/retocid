#!/bin/bash

sudo docker run \
    -t \
    -i \
    -p 8002:8000 \
    -v $PWD/site:/site \
    --name="retocid-shell" \
    --rm=true \
    iadb/retocid:v1 \
    /bin/bash
