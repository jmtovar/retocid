# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.db.models import Prefetch
from django.views.decorators.cache import cache_page
from django.http import HttpResponse

import fitbit
from models import MyUserFitbit

from collections import OrderedDict, defaultdict
from datetime import datetime, timedelta, date
from dateutil.rrule import *

from reports.models import DailyEntry, Version
import json

MAX_STEPS = 70000

# Create your views here.

@login_required(login_url=reverse_lazy('my:login'))
@cache_page(0)
def steps(request):
    u = request.user
    uf = MyUserFitbit.objects.filter(user = u).get()

    authd_client = fitbit.Fitbit(settings.FITAPP_CONSUMER_KEY, settings.FITAPP_CONSUMER_SECRET, resource_owner_key=uf.auth_token, resource_owner_secret=uf.auth_secret)
    steps = authd_client.time_series('activities/tracker/steps', period='7d')
    pasos = OrderedDict()
    for x in steps['activities-tracker-steps']:
        pasos[x['dateTime']] = x['value']
    #print steps
    context = {}
    context['pasos'] = pasos
    return render(request, 'activityw.html', context)

def list_dates(start, size):
    return list(rrule(DAILY, count=size, dtstart=start))

def detalle_promedios(request, anio, mes, dia, n, version, total=None, solo_activos=True):
    """Calcula la gráfica principal.
    """
    inicio = date(int(anio), int(mes), int(dia))
    fin = inicio + timedelta(int(n)-1)
    # Durante la semana, calcular en base al día anterior
    ayer = date.today() - timedelta(1)
    if fin <= ayer:
        numDays = int(n)
    else:
        if (ayer-inicio).days <= 0:
            numDays = 1
            fin = inicio
        else:
            numDays = (ayer - inicio).days + 1
            fin = ayer

    ldates = [inicio + timedelta(x) for x in range(0, numDays)]

    detail = []
    ver = Version.objects.filter(id = version).get()
    de = DailyEntry.objects.filter(source=0).filter(
            date__range=(inicio, fin)).filter(version=ver)
    activos = User.objects.filter(is_active=solo_activos).filter(pk__in=ver.users.all())
    con_fitbit = activos.filter(userfitbit__isnull=False)
    grupos = Group.objects.prefetch_related(
            Prefetch('user_set', activos),
            Prefetch('user_set', con_fitbit, to_attr='users'),
            Prefetch('users__userfitbit', to_attr='ufi'),
            Prefetch('users__ufi__dailyentry_set', de, to_attr='fechas_en_rango')
            )
    for grupo in grupos:
        item = {}
        item['name'] = grupo.name
        item['id'] = grupo.name
        item['data'] = []
        ri = {}
        for user in grupo.users:
            ri[user.username] = 0
            n = 0
            for v in user.ufi.fechas_en_rango:
                try:
                    if v.steps < MAX_STEPS:
                        ri[user.username] += v.steps
                        n += 1
                except DailyEntry.DoesNotExist:
                    pass

            if n != 0: ri[user.username] /= n
        for k, v in ri.items():
            item['data'].append( [k, v] )
        item['data'].sort(key=lambda x:x[1], reverse=True)
        detail.append(item)

    #print grupos
    def search_group(name):
        for g in grupos:
            if g.name == name:
                return g

    global_data = []
    for x in detail:
        item = {}

        inscritos = len(search_group(x['name']).users)
        total = search_group(x['name']).user_set.count()
        item['name'] = "%s-%d/%d" % (x['name'], inscritos, total )
        item['drilldown'] = x['name']
        avg = 0
        for v in x['data']:
            avg += v[1]
        if len(x['data']) != 0:
            if total:
                avg /= search_group(x['name']).user_set.count()
                item['y'] = avg
            else:
                avg /= len(x['data'])
                item['y'] = avg
        else:
            item['y'] = 0
        global_data.append(item)
    global_data.sort(key=lambda x:x['y'], reverse=True)

    return (global_data, detail, ldates)

def promedios(request, anio, mes, dia, n, version, total=None, solo_activos=True):
    global_data, detail, ldates = detalle_promedios(request, anio, mes, dia, n, version, total, solo_activos)
    context = {}
    context['from'] = ldates[0].strftime("%d/%m/%Y")
    context['to'] = ldates[-1].strftime("%d/%m/%Y")
    context['global_data'] = json.dumps(global_data)
    context['detail_data'] = json.dumps(detail)
    return render(request, 'promedios.html', context)

def promedio_total(request):
    g1, d1, _ = detalle_promedios(request, 2014, 11, 17, 7, 1)
    g2, d2, _ = detalle_promedios(request, 2014, 11, 24, 7, 2)
    g3, d3, _ = detalle_promedios(request, 2014, 12, 1, 7, 3)
    g4, d4, _ = detalle_promedios(request, 2014, 12, 8, 7, 4)
    g1.sort(key=lambda x:x['drilldown'])
    g2.sort(key=lambda x:x['drilldown'])
    g3.sort(key=lambda x:x['drilldown'])
    g4.sort(key=lambda x:x['drilldown'])

    countries = {}
    for el in d1:
        users = defaultdict(int)
        for user in el['data']:
            users[user[0]] += user[1]
        countries[el['id']] = users

    def add_steps(detail):
        for el in detail:
            users = countries[el['id']]
            for user in el['data']:
                users[user[0]] += user[1]
    add_steps(d2)
    add_steps(d3)
    add_steps(d4)

    corredores = []
    import operator
    for k, v in countries.items():
        sorted_x = sorted(v.items(), key=operator.itemgetter(1), reverse=True)
        corredores.append( (k, sorted_x[0][0], sorted_x[0][1]/4) )
    corredores = sorted(corredores, key=operator.itemgetter(2), reverse=True)

    for i in range(len(g1)):
        g1[i]['y'] = (g1[i]['y'] + g2[i]['y'] + g3[i]['y'] + g4[i]['y'])/4
        g1[i]['name'] = g1[i]['drilldown']

    g1.sort(key=lambda x:x['y'], reverse=True)

    context = {}
    context['global_data'] = json.dumps(g1)
    context['corredores'] = corredores
    return render(request, 'promedio_total.html', context)



def participantes(request):
    us = User.objects.filter(is_active=True).order_by('username')
    total = us.count()

    aprobados = User.objects.filter(is_active=True).filter(
            userfitbit__isnull=False).count()
    paises = OrderedDict()
    grupos = Group.objects.prefetch_related(
            Prefetch('user_set', us, to_attr='users'),
            Prefetch('user_set', 
                us.filter(userfitbit__isnull=False), to_attr='ufi'),
            Prefetch('users__userfitbit'),
            )

    for p in grupos:
        paises[p.name] = (p.users, len(p.ufi), len(p.users))

    context = {}
    context['paises'] = paises
    context['total'] = total
    context['aprobados'] = aprobados
    return render(request, 'participantes.html', context)

def cdf(request):
    from thinkstats.thinkstats2 import MakeCdfFromList, TrimmedMeanVar
    from thinkstats.thinkplot import Plot
    #, myplot, thinkstats2, continuous
    import matplotlib.pyplot as pyplot
    import math
    pyplot.close('all')
    response = HttpResponse(content_type='image/png')

    fig = pyplot.figure(1, figsize=(10,8))
    pyplot.subplot(111)
    pyplot.xlabel('$log_{10}(pasos)$')
    pyplot.ylabel('Percentiles')
    pyplot.title('CDF de pasos')

    #li = [math.log10(x.steps) for x in DailyEntry.objects.filter(source=0).all() if x.steps!=0]
    li = [x.steps for x in DailyEntry.objects.filter(source=0).all() if x.steps!=0 and x.steps<MAX_STEPS]
    mu, var = TrimmedMeanVar(li)
    sigma = math.sqrt(var)
    #print mu, var, sigma
    cdf = MakeCdfFromList(li)
    Plot(cdf)
    pyplot.savefig(response, format='png') #, dpi=300)

    return response

def lista_promedios(inicio, fin, name, version):
    #print inicio, fin
    de = DailyEntry.objects.filter(source=0).filter(date__range=(inicio, fin)).filter(version=version)
    usr = User.objects.filter(userfitbit__isnull=False)
    qs4 = Group.objects.prefetch_related(
            Prefetch('user_set'),
            Prefetch('user_set', usr, to_attr='users'),
            Prefetch('users__userfitbit', to_attr='ufi'),
            Prefetch('users__ufi__dailyentry_set', de, to_attr='fechas_en_rango')
            )
    li = []
    for grupo in qs4:
        por_usuario = {}
        for user in grupo.users:
            n = 0
            por_usuario[user.username] = 0
            for d_e in user.ufi.fechas_en_rango:
                try:
                    if d_e.steps < MAX_STEPS:
                        por_usuario[user.username] += d_e.steps
                        n += 1
                except DailyEntry.DoesNotExist:
                    pass
            if n != 0: por_usuario[user.username] /= n
        avg = sum(por_usuario.values())/len(por_usuario.values())
        print sum(por_usuario.values()), len(por_usuario.values()), avg
        li.append(avg)

    d = {}
    d['name'] = name
    d['data'] = li
    series = []
    series.append(d)
    return series

def comparacion(request):
    paises = [x.name for x in Group.objects.order_by('name').all()]
    series = []
    series += lista_promedios(date(2014, 11, 17), date(2014, 11, 23), 'Semana 1', 1)
    series += lista_promedios(date(2014, 11, 24), date(2014, 11, 30), 'Semana 2', 2)
    series += lista_promedios(date(2014, 12, 1), date(2014, 12, 7), 'Semana 3', 3)
    series += lista_promedios(date(2014, 12, 8), date(2014, 12, 14), 'Semana 4', 4)

    context = {}
    context['paises'] = json.dumps(paises)
    context['data_por_semana'] = json.dumps(series)
    return render(request, 'comparacion.html', context)

def promedios_tabla(request, id=1):
    de = DailyEntry.objects.filter(source=0).filter(date__range=(inicio, fin)).filter(version=version)
    usr = User.objects.filter(userfitbit__isnull=False)
    pais = Group.objects.prefetch_related(
            Prefetch('user_set'),
            Prefetch('user_set', usr, to_attr='users'),
            Prefetch('users__userfitbit', to_attr='ufi'),
            Prefetch('users__ufi__dailyentry_set', de, to_attr='fechas_en_rango')
            ).filter(id=id)

