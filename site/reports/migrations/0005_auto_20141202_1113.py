# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0004_auto_20141125_1158'),
    ]

    operations = [
        migrations.CreateModel(
            name='Version',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descripcion', models.CharField(max_length=64)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='dailyentry',
            name='version',
            field=models.ForeignKey(default=1, to='reports.Version'),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='dailyentry',
            unique_together=set([('user', 'date', 'source', 'version')]),
        ),
        migrations.AlterIndexTogether(
            name='dailyentry',
            index_together=set([('user', 'date', 'source', 'version')]),
        ),
    ]
