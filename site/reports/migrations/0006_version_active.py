# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0005_auto_20141202_1113'),
    ]

    operations = [
        migrations.AddField(
            model_name='version',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
