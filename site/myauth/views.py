# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy, reverse

# Create your views here.

from django.http import *
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
#from birthdayreminder.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.views.decorators.cache import cache_page

@cache_page(0)
def login_user(request):
    logout(request)
    username = password = ''
    context = {}
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('my:main'))
        else:
            context['error'] = u'El usuario no se encontró o es incorrecto.'
    return render_to_response('login.html', context,
            context_instance=RequestContext(request))

@login_required(login_url=reverse_lazy('my:login'))
def main(request):
    return render(request, "integration.html")

def myauth_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('my:login'))
