from django.conf.urls import patterns, include, url
from views import main, login_user, myauth_logout

urlpatterns = patterns('',
    url(r'^main/$', main, name='main'),
    url(r'^login/$', login_user, name='login'),
    url(r'^logout/$', myauth_logout, name='logout'),
)
