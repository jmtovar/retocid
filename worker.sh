#/bin/bash

sudo docker run -d \
        -e C_FORCE_ROOT=1 \
        -v $PWD/site:/site \
        --name="celery-worker" \
        iadb/retocid:celery \
        /usr/bin/python /site/retocid/manage.py celery worker \
        --app=retocid \
        --events \
        --loglevel=INFO \
        --logfile=/site/retocid/log/worker.log
